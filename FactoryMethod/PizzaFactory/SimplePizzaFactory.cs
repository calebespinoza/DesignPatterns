namespace DesignPatterns
{
    /* This is the factory where we create pizzas; it should be the only part of our application
       that refers to concrete Pizza classes
    */
    class SimplePizzaFactory
    {
        public IPizza createPizza(string typePizza)
        {
            IPizza pizza = null;

            if(typePizza.Equals("Cheese"))
            {
                pizza = new CheesePizza();
            } else if (typePizza.Equals("Pepperoni")) 
            {
                pizza = new PepperoniPizza();
            } else if (typePizza.Equals("Champignon")) 
            {
                pizza = new ChampignonPizza();
            } else if (typePizza.Equals("Veggie")) 
            {
                pizza = new VeggiePizza();
            }

            return pizza;
        }
    }
}