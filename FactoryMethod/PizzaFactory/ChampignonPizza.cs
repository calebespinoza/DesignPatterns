using System;

namespace DesignPatterns
{
    /* This is the concrete product. Each producto needs to implement the IPizza interface 
       and be concrete by the factory and handed back to the client.
    */
    class ChampignonPizza : IPizza
    {
        public void prepare()
        {
            Console.WriteLine("Adding mushrooms to the pizza.");
        }

        public void bake()
        {
            Console.WriteLine("Pizza is already baked");
        }

        public void cut()
        {
            Console.WriteLine("Pizza is going to be cut in 4 parts.");
        }
        
        public void box()
        {
            Console.WriteLine("Champignon Pizza is boxed and ready to go.");
        }
    }
}