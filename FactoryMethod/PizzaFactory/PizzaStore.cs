namespace DesignPatterns
{
    /* This is the client of the factory. PizzaStore now goes through the SimplePizzaFactory
       to get instances of IPizza.
    */
    class PizzaStore
    {
        SimplePizzaFactory factory;

        public PizzaStore(SimplePizzaFactory factory)
        {
            this.factory = factory;
        }

        public void orderPizza(string type)
        {
            /* The pizza variable receive an object of any classes that inheritance from IPizza interface.
               Product classes: ChessePizza || PepperoniPizza || VeggiePizza || ChampignonPizza 
            */
            var pizza = factory.createPizza(type);
            pizza.prepare();
            pizza.bake();
            pizza.cut();
            pizza.box();
        }

    }
}