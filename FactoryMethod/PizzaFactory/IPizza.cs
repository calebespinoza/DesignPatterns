using System;

namespace DesignPatterns
{
    /* This is the product of the factory pizza! */
    interface IPizza
    {
        /* We've defined Pizza as an interface class with some helpful
           implementations that can be implemented by the product classes
        */
        void prepare();
        void bake();
        void cut();
        void box();
    }
}