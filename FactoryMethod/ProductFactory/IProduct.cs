using System;

namespace DesignPatterns
{
    // The Factory Method pattern is applicable only when there is a products hierarchy
    public interface IProduct
    {
        string Operation();
    }
}