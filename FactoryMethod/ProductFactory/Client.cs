using System;

namespace DesignPatterns
{
    /* This is the client of Creator factory */
    class Client
    {
        public void main()
        {
            Console.WriteLine("App: Launched with the ConcreteCreator1.");
            ClientMethod(new ConcreteCreator1());

            Console.WriteLine("");

            Console.WriteLine("App: Launched with the ConcreteCreator2");
            ClientMethod(new ConcreteCreator2());
        }

        public void ClientMethod(Creator creator)
        {
            Console.WriteLine("Client: I'm not aware of the creator's, but it still works.\n"
            + creator.SomeOperation());
        }
    }
}