using System;

namespace DesignPatterns
{
    // This is a concrete product class
    class ConcreteProduct2 : IProduct
    {
        public string Operation()
        {
            return "{Return of ConcreteProduct2}";
        }
    }
}