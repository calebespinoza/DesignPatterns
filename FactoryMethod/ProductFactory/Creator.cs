using System;

namespace DesignPatterns
{
    /* Base Factory class. Note that the "factory" is merely a role for the class
     * It should have some core business logic which needs different products to be created
     */

     /* This factory class will be extended by subfactories classes */
    abstract class Creator
    {
        // Therefore we extract all product creation code to a special factory method.
        public abstract IProduct FactoryMethod(); // This method will be implemented by subfactories.

        public string SomeOperation(){
            var product = FactoryMethod();
            var result = "Creator: The same creator's code has just worked with " + product.Operation();
            return result;
        }
    }
}