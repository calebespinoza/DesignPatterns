namespace DesignPatterns
{
    // Concrete factories extend that method to produce different
    // kinds of products.

    // First products factory
    class ConcreteCreator1 : Creator
    {
        public override IProduct FactoryMethod()
        {
            return new ConcreteProduct1();
        }
    }
}