using System;

namespace DesignPatterns
{
    // Concrete factories extend that method to produce different
    // kinds of products.

    // Second products factory
    class ConcreteCreator2 : Creator
    {
        public override IProduct FactoryMethod()
        {
            return new ConcreteProduct2();
        }
    }
}