using System;

namespace DesignPatterns
{
    public class VlcPlayer : IAdvancedMediaPlayer
    {
        public void playVLC(string filename)
        {
            Console.WriteLine("Playing .vlc file. Name: {0}", filename);
        }

        public void playMP4(string filename)
        {
            // do nothing
        }
    }
}