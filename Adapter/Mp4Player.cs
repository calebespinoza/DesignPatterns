using System;

namespace DesignPatterns
{
    public class Mp4Player : IAdvancedMediaPlayer
    {
        public void playVLC(string filename)
        {
            // do nothing
        }

        public void playMP4(string filename)
        {
            Console.WriteLine("Playing .mp4 file. Name: {0}", filename);
        }
    }
}