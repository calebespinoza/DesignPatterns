using System;

namespace DesignPatterns
{
    public class MediaAdapter : IMediaPlayer
    {
        IAdvancedMediaPlayer advancedMusicPlayer;

        public MediaAdapter(string audioType)
        {
            if(audioType.Equals("vlc"))
            {
                advancedMusicPlayer = new VlcPlayer();
            }
            else if(audioType.Equals("mp4"))
            {
                advancedMusicPlayer = new Mp4Player();
            }
        }

        public void play(string audioType, string filename)
        {
            if(audioType.Equals("vlc"))
            {
                advancedMusicPlayer.playVLC(filename);
            }
            else if(audioType.Equals("mp4"))
            {
                advancedMusicPlayer.playMP4(filename);
            }
        }
    }
}