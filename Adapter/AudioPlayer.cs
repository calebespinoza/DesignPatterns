using System;

namespace DesignPatterns
{
    public class AudioPlayer : IMediaPlayer
    {
        MediaAdapter mediaAdapter;
        public void play(string audioType, string filename)
        {
            // in built support to play mp3 music files
            if(audioType.Equals("mp3"))
            {
                Console.WriteLine("Playing .MP3 file. Name: {0}", filename);
            } else if (audioType.Equals("vlc") || audioType.Equals("mp4"))
            {
                mediaAdapter = new MediaAdapter(audioType);
                mediaAdapter.play(audioType, filename);
            }
            else
            {
                Console.WriteLine("Invalid media. {0} format not supported", audioType);
            }
        }
    }
}