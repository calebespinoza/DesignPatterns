using System;

namespace DesignPatterns
{
    public interface IMediaPlayer
    {
        void play(string audioType, string filename);
    }
}