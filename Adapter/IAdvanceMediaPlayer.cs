using System;

namespace DesignPatterns
{
    public interface IAdvancedMediaPlayer
    {
        void playVLC(string filename);
        void playMP4(string filename);
    }
}