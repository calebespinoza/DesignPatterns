using System;

namespace DesignPatterns
{
    public class SellStockCommand : IOrderCommand
    {
        private StockReceiver abcStock;

        public SellStockCommand(StockReceiver abcStock)
        {
            this.abcStock = abcStock;
        }
        public void Execute()
        {
            abcStock.sell();
        }
    }
}