using System;

namespace DesignPatterns
{
    public class BuyStockCommand : IOrderCommand
    {
        private StockReceiver abcStock;

        public BuyStockCommand (StockReceiver abcStock)
        {
            this.abcStock = abcStock;
        }
        public void Execute()
        {
            abcStock.buy();
        }
    }
}