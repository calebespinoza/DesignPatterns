using System;
using System.Collections;
using System.Collections.Generic;

namespace DesignPatterns
{
    public class BrokerInvoker
    {
        private List<IOrderCommand> orderList = new List<IOrderCommand>();

        public void takeOrder(IOrderCommand order)
        {
            orderList.Add(order);
        }

        public void placeOrders()
        {
            foreach (var order in orderList)
            {
                order.Execute();                
            }
            orderList.Clear();
        }
    }
}