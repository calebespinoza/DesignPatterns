using System;

namespace DesignPatterns
{
    public class StockReceiver
    {
        private string name = "ABD";
        private int quantity = 10;

        public void buy ()
        {
            Console.WriteLine("Stock {0}, Quantity {1} bought", this.name, this,quantity);
        }

        public void sell ()
        {
            Console.WriteLine("Stock {0}, Quantity {1} sold", this.name, this, quantity);
        }
    }
}
