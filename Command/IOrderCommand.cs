using System;

namespace DesignPatterns
{
    public interface IOrderCommand
    {
        void Execute();
    }
}