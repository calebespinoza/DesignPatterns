﻿using System;

namespace DesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            //FACTORY METHOD PATTERN INSTANCE
            //Client cliente = new Client();
            //cliente.main();

            // FACTORY METHOD
            PizzaStore pizza = new PizzaStore(new SimplePizzaFactory());
            pizza.orderPizza("Cheese");
            pizza.orderPizza("Pepperoni");
            pizza.orderPizza("Veggie");
            pizza.orderPizza("Champignon");

            // COMMAND PATTERN INSTANCE
            /*StockReceiver abcStock = new StockReceiver();
            BuyStockCommand buyStockOrder = new BuyStockCommand(abcStock);
            SellStockCommand sellStockOrder = new SellStockCommand(abcStock);

            BrokerInvoker broker = new BrokerInvoker();
            broker.takeOrder(buyStockOrder);
            broker.takeOrder(sellStockOrder);

            broker.placeOrders();

            // ADAPTER PATTERN INSTANCE
            AudioPlayer audioPlayer = new AudioPlayer();
            audioPlayer.play("mp3", "Beyond the horizon.mp3");
            audioPlayer.play("mp4", "alone.mp4");
            audioPlayer.play("vlc", "far far away.vlc");
            audioPlayer.play("avi", "mind me.avi");*/
        }
    }
}
